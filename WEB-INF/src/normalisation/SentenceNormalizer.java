package normalisation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class SentenceNormalizer {
    private List<String> stoplist;
    private Lexique lexique;
    private Boolean accentSensitive;
    private Boolean verbose;
    private Boolean input;

    public SentenceNormalizer(List<String> stoplist, Lexique lexique, Boolean accentSensitive, Boolean input) {
        this.stoplist = stoplist;
        this.lexique = lexique;
        this.accentSensitive = accentSensitive;
        this.verbose = verbose;
        this.input = input;
    }

    public SentenceNormalizer(List<String> stoplist, Lexique lexique) {
        this(stoplist, lexique, false, true);
    }

    public String normalize(String sentence) throws IOException {

        //extraire les mots un a un
        StringTokenizer tokens = new StringTokenizer(sentence, " \t\n\r\f'");
        StringBuilder normalize = new StringBuilder();
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        while(tokens.hasMoreElements()){
            String word=tokens.nextToken().toLowerCase();
            if (!stoplist.contains(word)) {
                String result = lexique.get(word);
                Set<String> result2;
                if (result != null) {
                    normalize.append(result);
                } else if (word.length() > 2 && !(result2 = lexique.getWithPrefixSearch(word, 0.8, accentSensitive)).isEmpty()) {
                    if (result2.size() > 1) {
                        normalize.append(this.choice(word, new ArrayList<>(result2), input));
                    } else {
                        normalize.append(result2.iterator().next());
                    }

                } else if (word.length() > 2 && !(result2 = lexique.getWithLevenshtein(word, 2)).isEmpty()) {
                    if (result2.size() > 1) {
                        normalize.append(this.choice(word, new ArrayList<>(result2), input));
                    } else {
                        normalize.append(result2.iterator().next());
                    }
                } else {
                    normalize.append(word);
                }

                normalize.append(" ");
            }
        }

        return normalize.toString();
    }

    private String choice(String original, List<String> choices, BufferedReader br) throws IOException {
    	return choices.get(0);
    	/*
        if (!input) {
            return choices.get(0);
        } else {
            int i = 1;
            for(String r : choices) {
                System.out.println("\t["+i+"] "+r);
                i++;
            }
            System.out.println("\t["+i+"] "+original);
            System.out.println("Choisir un mot pour "+original+" : ");
            String choice = br.readLine();

            if ((i-1) == choices.size()) {
                return original;
            } else {
                return choices.get(Integer.parseInt(choice)-1);
            }
        }*/
    }
    
}
