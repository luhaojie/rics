package normalisation;

import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Set;

public class LexiqueTest extends TestCase {


    private Lexique lexique;

    public LexiqueTest() throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/corentinhembise/UTC/Cours/A18/LO17/java/src/tp3/test_lemmes.txt"));
        this.lexique = new Lexique(br);
    }

    public void testGet()
    {
        String found = this.lexique.get("archétype");
        assertEquals("archétype", found);
    }

    public void testPrefixAccentInsensitive()
    {
        Set<String> found = this.lexique.getWithPrefixSearch("archetype", 0.8, false);
        assertTrue(found.contains("archétype"));
    }

    public void testPrefixAccentSensitive()
    {
        Set<String> found = this.lexique.getWithPrefixSearch("archety", 0.8, true);
        assertFalse(found.contains("archétype"));
    }

    public void testLevenshteinDistance1True()
    {
        Set<String> found = this.lexique.getWithLevenshtein("maximel", 1);
        assertTrue(found.contains("maxi"));
    }

    public void testLevenshteinDistance1()
    {
        int distance = this.lexique.computeLevenshteinDistance("maximel", "maximal", true);
        assertEquals(1, distance);
    }


    public void testLevenshteinDistance2True()
    {
        Set<String> found = this.lexique.getWithLevenshtein("maxinel", 2);
        assertTrue(found.contains("maxi"));
    }

    public void testLevenshteinDistance2False()
    {
        Set<String> found = this.lexique.getWithLevenshtein("maxanel", 2);
        assertFalse(found.contains("maxi"));
    }

    public void testLevenshteinDistanceSwitch()
    {
        int distance = this.lexique.computeLevenshteinDistance("maxiaml", "maximal", true);
        assertEquals(1, distance);
    }
}
