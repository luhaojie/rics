import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import grammaire.tal_sqlLexer;
import grammaire.tal_sqlParser;
import normalisation.Lexique;
import normalisation.SentenceNormalizer;

public class Converter {
	public String normalize(String input) throws IOException, RecognitionException {
		
		BufferedReader br=null;
        BufferedReader phrase=null;
   
        
        //lire le fichier lemmes.txt and creer une instance of Lexique
        br = new BufferedReader(new FileReader("C:\\Users\\浩杰\\Desktop\\GI04\\LO17\\TD3\\lemmes.txt"));
        Lexique lexique = new Lexique(br);

        Scanner s = new Scanner(new File("C:\\Users\\浩杰\\Desktop\\GI04\\LO17\\TD4\\stoplist_P16.txt"));
        ArrayList<String> stoplist = new ArrayList<String>();
        while (s.hasNext()){
            stoplist.add(s.next());
        }
        s.close();

        String result=null;
        Set<String> result2=null;

        SentenceNormalizer normalizer = new SentenceNormalizer(stoplist, lexique, false, true);
        String normalized = normalizer.normalize(input);
        System.out.println(normalized);

        tal_sqlLexer lexer = new tal_sqlLexer(new ANTLRStringStream(normalized));
        CommonTokenStream tokenss = new CommonTokenStream(lexer);
        tal_sqlParser parser = new tal_sqlParser(tokenss);
        String sql = parser.listerequetes();
        
        return sql;
	}
}


