package grammaire;


import junit.framework.ComparisonFailure;
import junit.framework.TestCase;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestTal extends TestCase {

    private String orginal;
    private String normalized;
    private String expected;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        return Arrays.asList(new Object[][] {
                {"Je veux les articles qui parlent d'écologie.", "veux article parlent écologie.", "SELECT DISTINCT fichier FROM titretexte WHERE ((mot = 'écologie'))"},
                {"Quels sont les articles qui parlent d'écologie ?", "quels article parlent écologie", "SELECT DISTINCT fichier FROM titretexte WHERE ((mot = 'écologie'))"},
                {"Je veux les articles parlant de l'Argentine ou du Brésil.", "veux article parlent argentine ou brésil.", "SELECT DISTINCT fichier FROM titretexte WHERE ((mot = 'argentine') OR (mot = 'brésil'))"},
                {"Je veux les articles sur les énergies renouvelables.", "veux article sur energies renouvelable.", "SELECT DISTINCT fichier FROM titretexte WHERE ((mot = 'energies') AND (mot = 'renouvelable'))"},
                {"Article traitant des Serious Game et de la réalité virtuelle", "article traitan serious game et réalité virtuelle.", "SELECT DISTINCT fichier FROM titretexte WHERE ((mot = 'serious') AND (mot = 'game') AND (mot = 'réalité') AND (mot = 'virtuelle'))"},
                {"titre ou article mentionant les mots : rue, sport, competition", "titre ou article mentionant les mots : rue, sport, competition", "SELECT DISTINCT fichier FROM titretexte WHERE ((mot = 'rue') AND (mot = 'sport') AND (mot = 'competition')))"},

                //
                {"Combien d’auteurs ont écrit sur le sujet X ?", "combien auteur ont écrit sur sujet X ?", null},
                {"Trouve-moi des articles au sujet de l'énergie mais qui ne parlent pas du nucléaire.", "trouve-moi article sujet énergie ne parlent pas nucléaire.", "SELECT DISTINCT fichier from titretexte WHERE ((mot = 'énergie') AND (mot != 'nucléaire'))"},
                {"Quels sont les articles qui ont pour sujet le nucléaire ?", "quels sont article ont sujet nucléaire ?", null}
        });
    }

    public TestTal(String orginal, String normalized, String expected) {
        this.orginal = orginal;
        this.normalized = normalized;
        this.expected = expected;
    }

    @Before
    public void beforetest() {
        org.junit.Assume.assumeTrue("Ignoring test case", expected != null);
    }

    @Test
    public void testSqlGeneration() {

        tal_sqlLexer lexer = new tal_sqlLexer(new ANTLRStringStream(this.normalized));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        tal_sqlParser parser = new tal_sqlParser(tokens);
        String cleanedExpected = ("(" + this.expected + ")").replaceAll(" +", "");

        try {
            String sql = parser.listerequetes();
            String cleanedSql = sql.replaceAll(" +", "");
            assertEqualsIgnoreCase(cleanedExpected, cleanedSql);
        } catch (RecognitionException e) {
            fail(e.getMessage());
        }
    }

    private static void assertEqualsIgnoreCase(String expected, String actual) {
        assertEqualsIgnoreCase(null, expected, actual);
    }

    private static void assertEqualsIgnoreCase(String message, String expected, String actual) {
        if (expected != null && actual != null) {
            String cleanedExpected = expected.replaceAll(" +", "");
            String cleanedActual   = actual.replaceAll(" +", "");
            if (!expected.equalsIgnoreCase(actual)) {
                String cleanMessage = message == null ? "" : message;
                System.out.println(actual);
                throw new ComparisonFailure(cleanMessage, expected, actual);
            }
        }
    }
}
